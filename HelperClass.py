import configparser
import os

from selenium import webdriver
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

opt = webdriver.ChromeOptions()
opt.add_experimental_option("useAutomationExtension", False)
opt.add_experimental_option("excludeSwitches", ["enable-automation"])
opt.add_experimental_option('prefs', {
    'credentials_enable_service': False, 'profile.default_content_setting_values.geolocation': 2,
    'profile': {
        'password_manager_enabled': False
    }
})

args = [
    '--no-proxy-server',
    '--disable-web-security',
    '--disable-infobars',
    '--disable-notifications',
    'test-type',
    '--disable-popup-blocking',
    '--disable-session-crashed-bubble',
    '--disable-save-password-bubble',
    '--disable-permissions-bubbles',
    '--disable-extensions']
for arg in args:
    opt.add_argument(arg)

driver = webdriver.Chrome(executable_path=os.getcwd() + "/chromedriver.exe", chrome_options=opt)
waitFor5Secs = WebDriverWait(driver, 5)
driver.implicitly_wait(5)
driver.fullscreen_window()


def extractDataFromProperties():
    config = configparser.RawConfigParser()
    config.read('Credentials.properties')

    return config.get('Data', 'homePage'), config.get('Data', 'username'), config.get('Data', 'password')


def findElement(by):
    return driver.find_element(*by)


def findElements(by):
    return driver.find_elements(*by)


def waitFor5SecsVisibility(by):
    waitFor5Secs.until(expected_conditions.visibility_of_element_located(by))


def clickBy(by):
    waitFor5Secs.until(expected_conditions.element_to_be_clickable(by))
    findElement(by).click()
