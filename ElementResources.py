import random

from selenium.webdriver.common.by import By

from HelperClass import driver, clickBy, findElements

amenitiesCheckBox = By.CSS_SELECTOR, ".amenities__amenity-container .checkbox__box"
advantagesCheckBox = By.CSS_SELECTOR, ".labels .checkbox__box"


def checkAndUnfoldTab(classTarget):
    if len(driver.find_elements(By.CSS_SELECTOR,
                                classTarget + " .accordion__body.accordion__body--show-overflow")) == 0:
        clickBy(classTarget + ".accordion__headline")


def target(target):
    return By.CSS_SELECTOR, target + " .select__button"


def clickOption(which):
    element = By.CSS_SELECTOR, ".select-list-container button:nth-child({})".format(which)
    clickBy(element)


def clickAddQuantity(classTarget, quantity):
    element = By.CSS_SELECTOR, "{} .counter button:nth-last-of-type(1)".format(classTarget)
    for _ in range(quantity):
        clickBy(element)


def checkBox(target, quantity):
    if target == 'amenities':
        checkBoxQuantity = len(findElements(amenitiesCheckBox))
        for _ in range(quantity):
            findElements(amenitiesCheckBox)[random.randint(0, checkBoxQuantity - 1)].click()
    elif target == 'advantages':
        checkBoxQuantity = len(findElements(advantagesCheckBox))
        for _ in range(quantity):
            findElements(advantagesCheckBox)[random.randint(0, checkBoxQuantity - 1)].click()
