import random
import time

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from ElementResources import checkAndUnfoldTab, target, clickOption, clickAddQuantity, checkBox
from HelperClass import *

login = By.CSS_SELECTOR, ".header__sign-in-container"
loginContainer = By.CSS_SELECTOR, ".div.sign-in"
user = By.ID, "phoneNumberOrEmail"
passwd = By.ID, "password"
addListing = By.CSS_SELECTOR, ".header__add-container"
googleMap = By.CSS_SELECTOR, ".gmap"
copyAddressFromMap = By.CSS_SELECTOR, "[formcontrolname='copyGoogleAddress']"
totalSpace = By.CSS_SELECTOR, "#totalSpace input"
textArea = By.CSS_SELECTOR, ".additional-info textarea"
mobilePhoneLabel = By.CSS_SELECTOR, "#phoneNumber .select__button__placeholder"
fixedPrice = By.CSS_SELECTOR, ".total-price-container input"
submitListing = By.CSS_SELECTOR, ".analytics-publish-listing"

homePage, username, password = extractDataFromProperties()

driver.get(homePage)
time.sleep(1)
clickBy(login)
findElement(user).send_keys(username)
findElement(passwd).send_keys(password + Keys.ENTER)

clickBy(addListing)  # Clicks on " + დამატება " button

# ძირითადი ინფორმაცია Tab
checkAndUnfoldTab(".basic-info")  # Checks if ძირითადი ინფორმაცია Tab is unfolded

clickBy(target("#dealType"))  # გარიგების ტიპი
clickOption(1)  # იყიდება

clickBy(target("#propertyStatus"))  # ქონების სტატუსი
clickOption(2)  # ახალი აშენებული

clickBy(target("#project"))  # პროექტი
clickOption(1)  # ახალი პროექტი

clickBy(target("#condition"))  # მდგომარეობა
clickOption(2)  # ახალი რემონტი

# ადგილმდებარეობა Tab

checkAndUnfoldTab(".location")  # Checks if ადგილმდებარეობა Tab is unfolded

clickBy(target("#cityId"))  # ქალაქი / სოფელი
clickOption(1)  # თბილისი

clickBy(target("#parentDistrictId"))  # უბანი
clickOption(2)  # ვაკე

clickBy(target("#districtId"))  # ქვეუბანი
clickOption(1)  # ვაკე

driver.execute_script("arguments[0].scrollIntoView(true)", findElement(googleMap))
time.sleep(1)
clickBy(googleMap)  # Pins the center location of shown map
clickBy(copyAddressFromMap)  # Checks the box to copy address from the map

# ქონების დეტალები Tab

checkAndUnfoldTab(".space")  # Checks if ქონების დეტალები Tab is unfolded
findElement(totalSpace).send_keys(random.randint(100, 1000))  # Enters random value of space m^2

numberOfFloors = random.randint(1, 11)  # Generates the number 1-11 for

clickAddQuantity("#numberOfFloors", numberOfFloors)

floorCount = random.randint(1, 11)

while floorCount > numberOfFloors:  # Does the loop of setting random floor level before floor level is not less than total Floor number
    floorCount = random.randint(1, 11)

clickAddQuantity("#floor", floorCount)

clickAddQuantity("#numberOfRooms", random.randint(2, 6))  # Adds random rooms quantity

# კეთილმოწყობა Tab

checkAndUnfoldTab(".amenities")  # Checks if კეთილმოწყობა Tab is unfolded
checkBox("amenities", 6)  # Checks random boxes

# დამატებითი უპირატესობები Tab

checkAndUnfoldTab(".labels")  # Checks if დამატებითი უპირატესობები Tab is unfolded
checkBox("advantages", 5)  # Checks random boxes

# დამატებითი ინფორმაცია Tab

checkAndUnfoldTab(".additional-info")  # Checks if დამატებითი ინფორმაცია Tab is unfolded
findElement(textArea).send_keys("The information is filled by automation script")
phoneNumber = findElement(mobilePhoneLabel).text.replace(" ", "")  # Removes spaces from the text
assert phoneNumber == username  # Asserts if login phone number matches given number for listing

# გალერეა Tab

checkAndUnfoldTab(".gallery")  # Checks if გალერეა Tab is unfolded
element = By.CSS_SELECTOR, ".drop-file-container input"
for i in range(1, 9): # Uploads Picture from project directory ./pictureResources
    findElement(element).send_keys(os.getcwd()+"/pictureResources/pic{}.jpg".format(i))

# ფასი Tab

checkAndUnfoldTab(".price") # Checks if ფასი Tab is unfolded
findElement(fixedPrice).send_keys(random.randint(30000, 120000)) # Sets random price

# დამატება

time.sleep(2)
clickBy(submitListing)
